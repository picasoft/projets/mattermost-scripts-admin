## Détection des teams inactives sur mattermost

### Scripts bash utilisant l'[API v4](https://api.mattermost.com/)
* mattermost_api_liste_teams_anciennes.sh
* mattermost_api_dernier_message.sh

### Pré-requis
Pour que les scripts fonctionnent bien, il est necesaire d'utiliser le compte admin de l'instance mattermost. (Les identifiants de Picasoft se trouvent dans la section privée du wiki).
### Utilisation
* Depuis n'importe quel ordi sous linux, télecharger ce dossier, extraire et ouvrir un terminal dessus.
* Générer un token: `curl -i -d '{"login_id":"<adresse@email.com>","password":"<mot_de_passe>"}' https://team.picasoft.net/api/v4/users/login | grep token`
(Adaptez la commande pour votre instance)
* Lancez cette commande: `./mattermost_api_liste_teams_anciennes.sh "token"`
Exemple:	`./mattermost_api_liste_teams_anciennes.sh "h1z8..................5tmf"`
* Le script garde dans le fichier teams_anciennes.txt les détails des teams où le dernier message a été envoyé il y a plus de 1 an (id, display_name, email de l'admin de la team et la date du dernier message sur cette team)
* Si vous voulez modifier cette durée, il suffit de modifier cette ligne: `age_limite=31536000` (au début de mattermost_api_liste_teams_anciennes.sh). Il faut mettre une durée en secondes.




### Problèmes actuels

* Lorsqu'on génère un token, la commande qu'on a tappé (et donc le mot de passe du compte admin) reste dans l'historique! (~/.bash_history)


### Contribution
Toute contribution est bienvenue! N'hésitez pas à ouvrir une pull request ou à m'envoyer un mail sur amaldona@etu.utc.fr

### Licence
The MIT License  

Copyright (c) 2017 Andrés Maldonado

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

