#!/bin/bash
#Ce script garde dans le fichier teams_anciennes.txt les détails des teams où le dernier message a été envoyé il y a plus de 1 an (id, display_name, email de l'admin de la team et la date du dernier message sur cette team)

#Utilisation: 	./mattermost_api_liste_teams_anciennes.sh "token"
#Exemple:	./mattermost_api_liste_teams_anciennes.sh "h1z8..................5tmf"

#Pour obtenir un token: curl -i -d '{"login_id":"user@mail.com","password":"............"}' https://team.picasoft.net/api/v4/users/login | grep token


#Si il y a un bug, on peut enlever les commentaires des sections "TEST", ça peut donner des pistes 


#Un an=60×60×24×365=31536000 s
age_limite=31536000
#Variables pour mettre en forme plus facilement les messages
bold=$(tput bold)
normal=$(tput sgr0)

#Message d'aide
message_aide="#Ce script garde dans le fichier teams_anciennes.txt les détails des teams où le dernier message a été envoyé il y a plus de 1 an (id, display_name, email de l'admin de la team et la date du dernier message sur cette team)

	${bold}Utilisation:${normal}	./mattermost_api_liste_teams_anciennes.sh \"token\"
	${bold}Example:${normal}	./mattermost_api_liste_teams_anciennes.sh \"h1z8..................5tmf\"
	
	${bold}Pour obtenir un token:${normal} curl -i -d '{\"login_id\":\"user@mail.com\",\"password\":\"............\"}' https://team.picasoft.net/api/v4/users/login | grep token"

#check syntaxe
if [ "${#1}" != "26" ]; then
	echo "./mattermost_api_liste_teams_anciennes.sh: ${bold}Erreur de syntaxe! ${normal} le token doit avoir 26 caractères.
	$message_aide"
	exit
fi

#On récupere les ids de toutes les teams
string=`curl -i -H "Authorization: Bearer $1" https://team.picasoft.net/api/v4/teams`

#check erreur de connection
if [ "$string" = "" ]; then
	echo -e "\e[1;31mErreur de connection! \e[0m"
	exit
fi

#parser srting
ids=`echo $string | grep -o --regexp "\"id\":\"[^\"]*" | cut -c 7-32` 
display_names=`echo $string | grep -o --regexp "\"display_name\":\"[^\"]*" | cut -c 17-` 
emails=`echo $string | grep -o --regexp "\"email\":\"[^\"]*" | cut -c 10-`

#TEST
#echo "Liste des ids des teams:"$'\n'"$ids"
#echo "Liste des display_names des teams:"$'\n'"$display_names"
#echo "Liste des emails:"$'\n'"$emails"

IFS=$'\n' 
#On met les ids dans le tableau "tab_ids"
read -d '' -a tab_ids <<< $ids
#On met les display_names dans le tableau "tab_display_names"
read -d '' -a tab_display_names <<< $display_names
#On met les emails dans le tableau "tab_emails"
read -d '' -a tab_emails <<< $emails
unset IFS

#Normalement la longueur de tab_ids et tab_display_names est la même
tab_ids_length=`expr ${#tab_ids[@]}`

#effacer teams_anciennes.txt si il existe déjà
echo "" > teams_anciennes.txt

#TEST
#for (( i=0; i<$tab_ids_length; i++ )); do
#	echo "id: ${tab_ids[$i]}	display_name:${tab_display_names[$i]}	email:${tab_emails[$i]}"
#done

#récupérer la date du dernier message pour chaque team
for (( i=0; i<$tab_ids_length; i++ )); do
	date_actuelle=`date +%s`
	date_dernier_message=`./mattermost_api_dernier_message.sh "$1" "${tab_ids[$i]}"`
	date_format_humain=`date -d @$date_dernier_message`
	age_message=`expr $date_actuelle - $date_dernier_message`
	#TEST
	#echo "age message:$age_message age_limite=$age_limite"
	echo "Team: ${tab_display_names[$i]}"	
	if (( $age_message < $age_limite )); then
		#TEST
		#echo -e "./mattermost_api_dernier_message.sh \"$1\" \"${tab_ids[$i]}\""
		echo -e "Date du dernier message: \e[1;32m$date_format_humain\e[0m"
	else
		#TEST
		#echo -e "./mattermost_api_dernier_message.sh \"$1\" \"${tab_ids[$i]}\""
		echo -e "Date du dernier message: \e[1;31m$date_format_humain <-- team ancienne\e[0m"
		echo "id: ${tab_ids[$i]}	display_name:${tab_display_names[$i]}	email:${tab_emails[$i]}	date du dernier message:$date_format_humain" >> teams_anciennes.txt
	fi
done

