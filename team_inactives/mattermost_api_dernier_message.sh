#!/bin/bash
#Ce script est utilisé par mattermost_api_liste_teams_anciennes.sh
#Il retourne la date epoch du dernier message sur une team, en secondes

#Utilisation: 	./mattermost_api_dernier_message.sh "token" "id_equipe"
#Example:	./mattermost_api_dernier_message.sh "h1z8..................5tmf" "r1dse986zinr8xogtqirmqcoxa"

#On récupere la date epoch du dernier message de chaque channel de la team passée en paramètre
#(on utilise cut de manière à obtenir des dates en secondes et pas en millisecondes)
dates=`curl -i -H "Authorization: Bearer $1" https://team.picasoft.net/api/v4/teams/$2/channels | grep -o --regexp "\"last_post_at\":[0-9 ]*,"  | cut -c 16-25`

#check erreur de connection
if [ "$dates" = "" ]; then
	echo -e "\e[1;31mErreur de connection! \e[0m"
	exit
fi

#On trie les dates et on les met dans le tableau "dates_tri"
IFS=$'\n' dates_tri=($(sort <<<"$dates"))
unset IFS

#tester si tout a bien marché
#printf "%s\n" "${dates_tri[@]}"

nb_dates=`expr ${#dates_tri[@]}`
date_dernier_message=${dates_tri[$nb_dates-1]}

echo "$date_dernier_message"
