#!/bin/bash

read -s -p "Password: " password

export_sql_request_tmpl="""
SELECT json_agg(exp) FROM (
SELECT u.id as userid, u.username as username, u.email as email, to_timestamp((lastlogin.lastlogin/1000)) as last_login_date, userteams.nameteams as teams
FROM users u
INNER JOIN (
  SELECT userid, MAX(createat) as lastlogin 
  FROM audits a
  WHERE a.action = '/api/v4/users/login' AND a.extrainfo LIKE 'success%' 
  GROUP BY userid
  HAVING TO_TIMESTAMP(MAX(a.createat) / 1000) < TO_TIMESTAMP(TODAY_MIDNIGHT_EPOCH) - INTERVAL '3 years'
) lastlogin
ON 
   u.id = lastlogin.userid
INNER JOIN (
  SELECT teammembers.userid as userid, array_agg(teams.displayname) as nameteams
  FROM teammembers
  INNER JOIN teams ON teammembers.teamid = teams.id
  WHERE teammembers.deleteat = 0
  GROUP BY teammembers.userid
) userteams
ON
   u.id = userteams.userid
WHERE u.deleteat = 0) exp;
"""
today_midnight_epoch=$(date +'%s' -d 'today 00:00:00')

echo "$export_sql_request_tmpl" | sed "s/TODAY_MIDNIGHT_EPOCH/$today_midnight_epoch/" | docker exec -i mattermost-db psql -qAtX -U mattermost > inactive_accounts.json
if [ $? -ne 0 ]
then
	exit 1
fi

jq -c '.[]' inactive_accounts.json | while read i
do
	email_body=$(echo $i | gomplate -c ".=stdin:///data.yaml" -f email_inactive_account.tmpl)
	if [ $? -ne 0 ]
	then
		exit 1
	fi
	rcpt_to=$(echo $i | jq -r .email)
	if [ $? -ne 0 ]
	then
		exit 1
	fi
	if [ -z "$email_body" ]
	then
		echo "email body is empty for $rcpt_to"
		continue
	fi
	if [ -z "$rcpt_to" ]
	then
		echo "email rcpt to is empty"
		continue
	fi
	echo "$email_body" | curl -v --mail-from mattermost@picasoft.net --ssl --mail-rcpt "$rcpt_to" --user "mattermost:$password" --upload-file - smtp://mail.picasoft.net:587/team.picasoft.net
	if [ $? -ne 0 ]
	then
		echo "Failed to send email to $rcpt_to"
	fi
done
