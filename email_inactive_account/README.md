Ce script permet d'envoyer un mail à l'ensemble des comptes inactifs depuis au moins 3 ans dans le but de leur annoncer leur prochaine désactivation.

Pour le lancer il y a besoin de bash, de GNU date, de jq et de curl. Il faut aussi être sur la machine faisant tourner Mattermost à l'aide de Docker et avoir accès à son conteneur de base de donnée.

Il faudra aussi installer [gomplate](https://docs.gomplate.ca/), un outil de template utilisable en CLI. On peut utiliser ces commandes pour l'installer (changer pour la denrière version).
```
curl -o gomplate -L https://github.com/hairyhenderson/gomplate/releases/download/v3.11.7/gomplate_linux-amd64
sudo install gomplate /usr/local/bin/gomplate
rm gomplate
```

On peut ensuite lancer le script, il faudra spécifier le mot de passe du compte Mattermost sur le ldap afin d'envoyer les mails.
```
./send_email_inactive_account.sh
```
