#!/usr/bin/env python

# env TOKEN is the token
# $1 is your username
# $2 is a CSV with usernames and additional arguments.
# A column must be named userid and contain the user id that must receive the message.
# $3 is a message template
#    where @argument@ will be replaced by CSV's values

import requests
import os
import sys

BASE_URL = "https://team.picasoft.net/api/v4"

def get_auth():
    return {"Authorization": f"Bearer {os.environ['TOKEN']}"}

def csv_line(line) -> list[str]:
    return line[0:-1].split(",")

def csv_load(filename) -> list[dict[str,str]]:
    data = list()
    with open(filename) as file:
        headers = csv_line(file.readline())
        for line in file:
            values = csv_line(line)
            row = dict()
            for i in range(len(headers)):
                row[headers[i]] = values[i]
            data.append(row)
    return data

username = sys.argv[1]

data = csv_load(sys.argv[2])

message = open(sys.argv[3]).read()

me = requests.post(
    f"{BASE_URL}/users/usernames", json=[username], headers=get_auth()
).json()[0]

for row in data:
    local_message = message
    for key, value in row.items():
        local_message = local_message.replace(f"@{key}@", value)

    r = requests.post(
        f"{BASE_URL}/channels/direct",
        json=[row["userid"], me["id"]],
        headers=get_auth(),
    )
    r = requests.post(
        f"{BASE_URL}/posts",
        json={
            "channel_id": r.json()["id"],
            "message": local_message,
        },
        headers=get_auth(),
    )
    print(f"Message sent to {row['username'] if 'username' in row else user['userid']}")

