Hello @@username@,
Picasoft fait un petit ménage de nouvelle année de Mattermost,
Et nos robots surpuissants (et surarmés) viennent de découvrir que l'équipe [@team@](https://team.picasoft.net/@team@)
n'a pas eu d'activité depuis au moins deux ans.
Si tu nous confirmes que cette équipe peut-être archivée, on s'exécute !
Si tu préfères qu'elle soit supprimée, c'est une option aussi.
Dans tous les cas, on attend un mois à partir de l'envoi de ce message pour faire quelque chose.
Bonne journée !
Pomme pour Picasoft
