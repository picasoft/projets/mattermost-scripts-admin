#/usr/bin/env python

import sys
import time
import requests
import datetime

def get_auth(token: str):
    return {"Authorization": f"Bearer {token}"}

def last_activity_recent_enough(sessions: str, oldTime: str) -> bool:
	for session in sessions:
		if oldTime < int(session["last_activity_at"]):
			return True

def list_active_users(oldTimeHuman: str, username: str, token: str, baseUrl: str):
    all_users = []
    users = True
    i = 0
    print("Retrieving all users...", file=sys.stderr)
    while users:
        users = requests.get(
            f"{baseUrl}users",
            params={"per_page": "1000", "page": str(i), "active": "true"},#, "sort": "last_activity_at"},
            headers=get_auth(token),
        ).json()
        print(f"{i}: {len(users)}")
        all_users.extend(users)
        i = i + 1

    print(f"{len(all_users)} users.", file=sys.stderr)
    print("Filtering users on sessions...", file=sys.stderr)
    active_users = []

    for i in range(len(all_users)):
        user = all_users[i]
        if i % 100 == 0:
            print(i)
        sessions = requests.get(
            f"{baseUrl}users/{user['id']}/sessions", headers=get_auth(token)
        ).json()

        if last_activity_recent_enough(sessions, oldTime):
            active_users.append(user)

    return active_users

if __name__ == "__main__":
    username = "ppom"
    token = "ch1ew66o8pnsxyzepo4opae8oh"
    # ex: 15-05-2022
    oldTimeHuman = sys.argv[1]
    oldTime = (
        time.mktime(datetime.datetime.strptime(oldTimeHuman, "%d-%m-%Y").timetuple()) * 1000)
    baseUrl = "https://team.picasoft.net/api/v4/"

    active_users = list_active_users(oldTimeHuman, username, token, baseUrl)
    print()
    print(len(active_users), 'active_users.')
    print('active_users:', [ u["username"] for u in active_users ])

    with open(f"active_since_{oldTimeHuman}.txt", "w+") as f:
        for u in active_users:
            f.write(u["username"]+"\n")
