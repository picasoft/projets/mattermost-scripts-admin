## Envoyer un message privé aux personnes "actives"

Ce script (à l'arrache, pas comme les beaux scripts de Kyâne) permet d'envoyer un message privé depuis un compte arbitraire à toutes les personnes s'étant connectées au moins une fois depuis une date donnée.

Cas d'usage : annoncer quelque chose d'important sur le Mattermost, sans pouvoir utiliser la bannière d'annonce qui est réservée à la version entreprise.
Attention : les messages ne peuvent pas être envoyés depuis un bot, mais le sont directement depuis le compte mentionné.

Pour faire fonctionner le script il suffit de renseigner les identifiants directement dans le code. 

Ce script utilise l'API v4 et a besoin d'un compte avec les privilèges d'administrateur pour fonctionner.
