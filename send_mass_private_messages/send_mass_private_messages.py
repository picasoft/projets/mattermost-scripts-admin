#/usr/bin/env python

import requests
import time
import datetime

from list_active_users import list_active_users

message = ""
username = ""
token = "" # À générer dans son "Profil"
oldTimeHuman = "10-05-2022"
baseUrl = "https://team.picasoft.net/api/v4/"

def get_auth():
    return {"Authorization": f"Bearer {token}"}

me = requests.post(
    f"{baseUrl}users/usernames", json=[username], headers=get_auth()
).json()[0]

active_users = list_active_users(oldTimeHuman, username, token, baseUrl)

for user in active_users:
    sessions = requests.get(
        f"{baseUrl}users/{user['id']}/sessions", headers=get_auth()
    ).json()
    for session in sessions:
        if int(oldTime) < int(session["last_activity_at"]):
            r = requests.post(
                f"{baseUrl}channels/direct",
                json=[user["id"], me["id"]],
                headers=get_auth(),
            )
            r = requests.post(
                f"{baseUrl}posts",
                json={
                    "channel_id": r.json()["id"],
                    "message": f"### Salut {user['username']} ! :D \n{message}",
                },
                headers=get_auth(),
            )
            print(f"Message sent to {user['username']}")
            break
