## Masquer les canaux de groupe et les canaux privés

Ce script (à l'arrache, pas comme les beaux scripts de Kyâne) permet juste de masquer tous les canaux privés et les conversations de groupe d'un utilisateur.

Cas d'usage : beaucoup de canaux accumulés au fil des années et flemme de spammer la croix dans l'interface.

Les canaux ne sont évidemment pas supprimés.

Pour faire fonctionner le script il suffit de renseigner les identifiants directement dans le code. 

Ce script utilise l'API v4. On remarquera que la gestion de l'affichage des canaux privés est un peu ésotérique : il faut passer à l'API le nom du canal, amputé des underscores de l'identifiant de l'utilisateur dont on souhaite nettoyer les canaux. Le nom d'un canal privé est habituellement formé comme suit : <id_me>__<id_other>. Ici, on passera seulement <id_other> à l'API.
