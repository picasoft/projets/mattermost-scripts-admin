#!/usr/bin/env python

import requests
baseUrl = 'https://team.picasoft.net/api/v4/'
def get_auth():
    return { 'Authorization': 'Bearer ' + token }

r = requests.post(
    baseUrl + 'users/login',
    json = {
        'login_id': '',
        'password': ''
    })

token = r.headers['Token']

me = requests.post(
    baseUrl + 'users/usernames',
    json = [ 'quentinduchemin' ],
    headers = get_auth()
).json()[0]

teams = requests.get(
    baseUrl + 'users/{}/teams'.format(me['id']),
    headers = get_auth()
).json()

chans = requests.get(
    baseUrl + '/users/{0}/teams/{1}/channels'.format(me['id'], teams[0]['id']),
    headers = get_auth()
).json()

for chan in chans:
    t = chan['type']
    if t == 'D' or t == 'G':
        r = requests.put(
            baseUrl + 'users/{}/preferences'.format(me['id']),
            json = [{
                'user_id': me['id'],
                'category': 'direct_channel_show' if t == 'D' else 'group_channel_show',
                'name': chan['name'].replace(me['id'], '').replace('__', ''),
                'value': 'false'
            }],
            headers = get_auth()
        )
        print('Removed {}'.format(chan['name'].replace(me['id'], '').replace('__', '')))

