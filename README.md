# Scripts Mattermost de Picasoft

Ce *repository* héberge plusieurs scripts qui facilitent l'administration de Mattermost pour Picasoft.

- `team_inactives` permet de détecter les équipes inactives
- `disable_notifications` permet de désactiver les notifications mail d'une adresse mail donnée (utile pour de vieux comptes avec des adresses etu)
- `hide_private_chans` permet de masquer sur l'interface les canaux privés d'un utilisateur (à des fins d'ergonomie)
- `send_mass_private_messages` permet d'envoyer un message privé à tous les utilisateurs actifs depuis une date donnée
