#!/bin/bash

read -s -p "Mattermost token: " mattermost_token
read -p "Alert date: " alert_date

export_sql_request_tmpl="""
SELECT u.id as userid
FROM users u
INNER JOIN (
  SELECT userid, MAX(createat) as lastlogin 
  FROM audits a
  WHERE a.action = '/api/v4/users/login' AND a.extrainfo LIKE 'success%' 
  GROUP BY userid
  HAVING TO_TIMESTAMP(MAX(a.createat) / 1000) < TO_TIMESTAMP(ALERT_DATE_MIDNIGHT_EPOCH) - INTERVAL '3 years'
) lastlogin
ON 
   u.id = lastlogin.userid
INNER JOIN (
  SELECT teammembers.userid as userid, array_agg(teams.displayname) as nameteams
  FROM teammembers
  INNER JOIN teams ON teammembers.teamid = teams.id
  WHERE teammembers.deleteat = 0
  GROUP BY teammembers.userid
) userteams
ON
   u.id = userteams.userid
WHERE u.deleteat = 0;
"""

alert_date_midnight_epoch=$(date -d "$alert_date" +'%s')
inactive_users=$(echo "$export_sql_request_tmpl" | sed "s/ALERT_DATE_MIDNIGHT_EPOCH/$alert_date_midnight_epoch/" | docker exec -i mattermost-db psql -qAtX -U mattermost)

for userid in $(echo $inactive_users)
do
	curl -X DELETE -H "Authorization: Bearer $mattermost_token" "https://team.picasoft.net/api/v4/users/$userid"
done
