# Disable notifications

This script simply disable email notifications for a user and send a private message. It is useful when people have a wrong or deleted email address associated to their account (eg. old UTC student email).

This script need some requirements that you can install with `pip install requirements.txt`.

The script need access to an admin account on the Mattermost server. To provide authentication information there is 2 way :
- envrionment variables : `MM_URL` (eg. `https://team.picasoft.net`), `MM_USER` and `MM_PASSWORD`
- CLI arguments : `--mm-url`, `--mm-user` and `--mm-password`

Also, there is a required CLI argument : `--email` which is the mail of the user that need to have notifications disabled.
