#!/usr/bin/python3
# coding=utf-8

"""Remove notifications for a mattermost user."""

# Imports
import argparse
import os
from urllib.parse import urlparse
import sys
import json
from mattermostdriver import Driver

# Constants
NOTIF_MESSAGE = """Salut ! Ceci est un message "automatique" de Picasoft :).
On a constaté des erreurs d'envois des mails de notifications de Mattermost vers ton adresse mail `[[EMAIL]]`. Un robot a donc désactivé les notifications par mails de ton compte Mattermost.
Cela peut venir du fait que ton adresse n'existe plus, ou encore d'un problème venant de ton serveur mail ou de celui de Picasoft. Si tu penses que l'on fait erreur et/ou pour avoir plus d'informations, n'hésite pas à nous contacter sur notre équipe Mattermost `Picasoft` ou par mail à `picasoft@assos.utc.fr`."""


def mattermost_connect(args):
    """
    Instanciate a Mattermost connector.
    :param args: CLI arguments
    :return mat_api: A Mattermost APi connector
    """
    # Extract Mattermost server informations
    if args.mm_url is not None:
        mm_url = args.mm_url
    elif os.getenv('MM_URL') is not None:
        mm_url = os.getenv('MM_URL')
    else:
        print('You need to specify --mm-url or MM_URL environment variable !')
        sys.exit(1)
    if args.mm_user is not None:
        mm_user = args.mm_user
    elif os.getenv('MM_USER') is not None:
        mm_user = os.getenv('MM_USER')
    else:
        print('You need to specify --mm-user or MM_USER environment variable !')
        sys.exit(1)
    if args.mm_password is not None:
        mm_password = args.mm_password
    elif os.getenv('MM_PASSWORD') is not None:
        mm_password = os.getenv('MM_PASSWORD')
    else:
        print('You need to specify --mm-password or MM_PASSWORD environment variable !')
        sys.exit(1)
    # Parse Mattermost URL
    o = urlparse(mm_url)
    if o.port is None:
        if o.scheme == 'https':
            mm_port = 443
        elif o.scheme == 'http':
            mm_port = 80
        else:
            print('Impossible to get Mattermost server port for ' + mm_url)
            sys.exit(1)
    else:
        mm_port = o.port

    # Connect to Mattermost
    mat_api = Driver({
        'url': o.hostname,
        'login_id': mm_user,
        'password': mm_password,
        'scheme': o.scheme,
        'port': mm_port
    })
    mat_api.login()
    return mat_api


def get_user_by_email(email, conn):
    """
    Get a Mattermost user by its email address.
    :param email: Email to search for
    :param conn: Connector for Mattermost API
    :return user: Mattermost user
    """
    # Search the user
    try:
        res = conn.client.make_request('post', '/users/search', options={'term': email})
    except Exception as e:
        print(e)
        print('Error when requesting Mattermost API to search user ' + email)
        sys.exit(1)
    data = json.loads(res.text)
    # Check in results for a user with the exact mail adress
    for user in data:
        if user['email'] == email:
            return user
    return None


def notify(user, message, conn):
    """
    Send a private message to a Mattermost user.
    :param user: Mattermost user object
    :param message: Message to send
    :param conn: Connector for Mattermost API
    """
    # Get current user
    try:
        res = conn.client.make_request('get', '/users/me')
    except Exception as e:
        print(e)
        print('Error when requesting Mattermost API to get current user.')
        sys.exit(1)
    data = json.loads(res.text)

    # Create private message channel
    try:
        res = conn.client.make_request('post', '/channels/direct', options=[data['id'], user['id']])
    except Exception as e:
        print(e)
        print('Error when requesting Mattermost API to create private conversation.')
        sys.exit(1)
    data = json.loads(res.text)

    # Post a message
    try:
        conn.client.make_request('post', '/posts', options={'channel_id': data['id'], 'message': message})
    except Exception as e:
        print(e)
        print('Error when requesting Mattermost API to send a private message.')
        sys.exit(1)


def disable_notifications(user, conn):
    """
    Disable mail notifications for a Mattermost user.
    :param user: Mattermost user object
    :param conn: Connector for Mattermost API
    """
    # Patch user to disable mail notifications
    try:
        conn.client.make_request('put', '/users/' + user['id'] + '/patch', options={'notify_props': {'email': 'false'}})
    except Exception as e:
        print(e)
        print('Error when disabling notifications for user ' + user['id'] + ' with Mattermost API.')
        sys.exit(1)


def main():
    """Run the main function."""
    # Parse CLI arguments
    parser = argparse.ArgumentParser(description='Remove Matttermost notifications for an email.')
    parser.add_argument('--email', help='Email of the user', required=True)
    parser.add_argument(
        '--mm-url',
        help='Full URL of the Mattermost instance (eg. https://team.picasoft.net). Can be specified using MM_URL environment variable.',
        required=False
    )
    parser.add_argument(
        '--mm-user',
        help='Username or email of a Mattermost admin account. Can be specified using MM_USER environment variable.',
        required=False
    )
    parser.add_argument(
        '--mm-password',
        help='Password of the Mattermost admin account. Can be specified using MM_PASSWORD environment variable.',
        required=False
    )
    args = parser.parse_args()

    # Connect to Mattermost
    mat_api = mattermost_connect(args)

    # Get the user
    user_email = args.email
    user = get_user_by_email(user_email, mat_api)
    if user is None:
        print('Impossible to find a user matching ' + user_email)
        sys.exit(1)

    # Disable notifications
    disable_notifications(user, mat_api)

    # Send notification
    msg = NOTIF_MESSAGE.replace('[[EMAIL]]', user_email)
    notify(user, msg, mat_api)


if __name__ == '__main__':
    main()
